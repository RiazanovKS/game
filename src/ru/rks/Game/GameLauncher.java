package ru.rks.Game;

/**
 * Класс проверяет работоспособность классов GuessGame и Player.
 *
 * @author Рязанов К.С.
 */
public class GameLauncher {
    public static void main (String[]args){
        GuessGame game = new GuessGame();
        game.startGame();
    }

}

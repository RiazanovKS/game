package ru.rks.Game;
/**
*Класс представляет игрока
 * @author Рязанов К.С.
*/
public class Player {
    int number = 0;

    public Player(){
        number = (int) (Math.random() * 10);
    }

    public Player(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return ("Игрок думает - "+ number);
    }
}

package ru.rks.Game;

/**
 * Класс представляет игру "Угадай число"
 *
 * @author Рязанов К.С.
 */
public class GuessGame {
    int targetNumber;

    Player p1 = new Player();
    Player p2 = new Player();
    Player p3 = new Player();

    public GuessGame(int targetNumber) {
        this.targetNumber = targetNumber;
    }

    public GuessGame() {
        targetNumber= (int)(Math.random()*10);
    }

    /**
     * Метод выводит результаты игры.
     */


    public void startGame() {
        System.out.println("Число для угадывания - "+ targetNumber);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        if(isGuessed(p1)||isGuessed(p2)||isGuessed(p3)) {
            System.out.println("У нас есть победитель");
            System.out.println("Угадал ли игрок 1? - " + isGuessed(p1));
            System.out.println("Угадал ли игрок 2? - " + isGuessed(p2));
            System.out.println("Угадал ли игрок 3? - " + isGuessed(p3));
            System.out.println("Игра окночнена");
        }else{
            System.out.println("Очень жаль, но никто не угадал");
            System.out.println("Игроки начнут снова");
        }
    }

    /**
     * Метод возвращает булевое значение в зависимости от результата сравнения числа игрока и числа для угадывания
     *
     * @param player
     * @return true- если числа совпадают, false- если нет.
     */
    public boolean isGuessed(Player player) {
        boolean isPlayerRight= false;
        if (player.number == targetNumber) {
            isPlayerRight = true;
        }
        return isPlayerRight;
    }
}
